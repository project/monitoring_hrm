Monitoring — Heart Rate Monitor

https://www.drupal.org/project/monitoring_hrm

# Configuration

There is currently no configuration UI. You can choose to export configuration
and edit it manually, or you can add a configuration override in your
settings.php file:

```php
$config['monitoring_hrm.settings']['endpoint_key'] = 'mySecret32CharacterKeyABCDEF1234';
```

# Usage

Navigate to https://mysite.tld/healthz?token=mySecret32CharacterKeyABCDEF1234

This response will return a HTTP code 200 if there are no Monitoring sensor
failures. If there are failures the response code will be 500. The response
contents itself is the same: a JSON response with a counter of number of
failures.

This response can be easily hooked up to Pingdom or Statuscake, where you can
alert site maintenance team of problems. 

# License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
