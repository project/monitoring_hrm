<?php

declare(strict_types = 1);

namespace Drupal\monitoring_hrm\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResult;
use Drupal\monitoring\SensorRunner;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Monitoring controller.
 */
class MonitoringHrmController extends ControllerBase {

  /**
   * The sensor runner.
   *
   * @var \Drupal\monitoring\SensorRunner
   */
  protected $sensorRunner;

  /**
   * Constructs a \Drupal\monitoring\Form\SensorDetailForm object.
   *
   * @param \Drupal\monitoring\SensorRunner $sensor_runner
   *   The factory for configuration objects.
   */
  public function __construct(SensorRunner $sensor_runner) {
    $this->sensorRunner = $sensor_runner;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('monitoring.sensor_runner')
    );
  }

  /**
   * API endpoint to get health for the site.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response.
   */
  public function healthEndpoint(): JsonResponse {
    // Results may be cached from sensor runner.
    $results = $this->sensorRunner->runSensors();
    $resultFailures = array_filter($results, function (SensorResult $result): bool {
      return $result->getStatus() === SensorResultDataInterface::STATUS_CRITICAL;
    });

    $cacheability = (new CacheableMetadata())
      // Uncacheable response.
      ->setCacheMaxAge(0);

    $response = (new CacheableJsonResponse())
      ->setData(['count_failures' => count($resultFailures)])
      ->addCacheableDependency($cacheability)
      ->setStatusCode(count($resultFailures) > 0 ? 500 : 200);

    return $response;
  }

}
