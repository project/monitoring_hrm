<?php

declare(strict_types = 1);

namespace Drupal\monitoring_hrm\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Check if a token is present in GET args.
 */
class MonitoringHrmEndpointAccessCheck implements AccessInterface {

  /**
   * Name of a GET param to get the key.
   */
  protected const KEY_NAME = 'token';

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new MonitoringHrmEndpointAccessCheck.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Determine access for the endpoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming HTTP request object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   An access result.
   */
  public function access(Request $request): AccessResult {
    $settings = $this->configFactory->get('monitoring_hrm.settings');
    $expectedKey = $settings->get('endpoint_key');
    $userKey = $request->query->get(static::KEY_NAME);
    // Substring caps the value length.
    return AccessResult::allowedIf(empty($expectedKey) || (is_string($userKey) && substr($userKey, 0, 32) == $expectedKey))
      ->addCacheContexts(['url.query_args:' . static::KEY_NAME])
      ->addCacheableDependency($settings);
  }

}
